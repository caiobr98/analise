<?php

namespace App\Models\Beneficiario;

use App\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Beneficiario extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'QBeneficiario';

    protected $primaryKey = 'fk_idCliente';

    protected $fillable = [
        'fk_idCliente',
        'fk_idGrupo',
        'fk_idGrupoOperadora',
        'fk_idBeneficiario' ,
        'idOperadora' ,
        'idOperadoraApolice',
        'idOperadoraEmpresa',
        'idMatricula' ,
        'idMatriculaTitular',
        'idMatriculaEmpresa',
        'idParentesco',
        'idSexo' ,
        'idEstadoCivil' ,
        'idCorPele',
        'idIMC' ,
        'Nome' ,
        'NomeTitular',
        'CPF' ,
        'CPFTitular',
        'dtNascimento',
        'Peso' ,
        'Altura' ,
        'IMC' ,
        'eMail',
        'Telefone',
        'dtCadastro',
        'dtAtualizacao',
        'autenticado',
        'Senha'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'Senha'
    ];
}
