<?php

namespace App\Models\QUestao;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Questao extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'Questao';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'idCategoria',
        'idGrupo',
        'idFaixaEtaria',
        'idTipoResposta',
        'idDescritivo',
        'idObrigatorio',
        'Questao',
        'TextoDescritivo',
        'Ordem',
        'Score',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
