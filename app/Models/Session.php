<?php

namespace App\Models;

use App\Models\BaseModel;

class Session extends BaseModel
{
    protected $table = 'sessions';
}
