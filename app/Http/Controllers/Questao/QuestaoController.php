<?php

namespace App\Http\Controllers\Questao;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Questao\Questao;

class QuestaoController extends Controller
{
    public function __construct()
    {
        $this->middleware('autenticado:1');
    }

    public function index()
    {
        $q = Questao::all();
        return view('questao.index');
    }
    
    public function create()
    {

    }

    public function store(Request $request)
    {

    }
    
    public function edit($id)
    {
 
    }

    public function update(Request $request, $id)
    {

    }
    
    public function destroy($id)
    {

    }
}
