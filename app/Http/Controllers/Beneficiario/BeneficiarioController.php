<?php

namespace App\Http\Controllers\Beneficiario;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Beneficiario\Beneficiario;
use Illuminate\Support\Facades\Hash;
use Session;

class BeneficiarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('autenticado:1');
    }

    public function index()
    {
        return view('beneficiario.login');
    }

    public function logar(Request $request){
        $data = $request->validate([
            'cpf' => 'required|string|max:20',
            'senha' => 'required|string'
        ]);
        
        $b = Beneficiario::where('CPF', $data['cpf'])->where('Senha', $data['senha'])->first();
        
        // dd($beneficiario);
        if($b){
            Session::push('id', $b->id);
            return redirect('/beneficiario/dependentes/'.$b->id);
        } else {
            return back()->with('error', 'Beneficiário não encontrado');
        }
    }

    public function dependentes($id)
    {
        $b = Beneficiario::find($id);

        $beneficiario = Beneficiario::where('CPFTitular', $b->CPFTitular)
                ->get();
        return view('beneficiario.menu')->with([
            'beneficiario' => $beneficiario 
        ]);
    }
    
    public function create()
    {

    }

    public function store(Request $request)
    {

    }
    
    public function edit($id)
    {
 
    }

    public function update(Request $request, $id)
    {

    }
    
    public function destroy($id)
    {

    }
}
