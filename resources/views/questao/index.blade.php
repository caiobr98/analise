@extends('layouts.appbeneficiario')
@section('title') Formulários @endsection
@section('content')
    <div class="">
        <ol class="breadcrumb">
            <li><a href="{{ route('painel', Session::get('id')[0]) }}"><i class="fa fa-home"></i> Dependentes  </a></li>
            <li class="active">
               / Formulários
            </li>
        </ol>

        <div class="row">
            <div class="col-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Formulários</h2>
                        {{-- <a class="pull-right btn btn-success" href="{{url('moleculas/create')}}">
                            Nova Molécula
                        </a>
                        <div class="clearfix"></div> --}}
                    </div>
                    <div class="x_content">
                        @include('parts.messages')

                        <table class="table table-striped data_table">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Pergunta</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{-- @foreach($beneficiario as $b) --}}
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <a class="btn btn-primary" href="">
                                            <i class="fa fa-edit"></i> Formulário
                                        </a>
                                    </td>
                                </tr>
                            {{-- @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
