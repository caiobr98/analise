<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Identificação</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('asset_login/css/util.css')}}"> --}}
	<link rel="stylesheet" type="text/css" href="{{asset('css/identificacao.css')}}">
</head>
<!------ Include the above in your HEAD tag ---------->
<body>
    <div class="sidenav">
        <div class="login-main-text">
           <h2>Application<br> Login Page</h2>
           <p>Login or register from here to access.</p>
        </div>
    </div>
    <div class="main">
        <div class="col-md-6 col-sm-12">
            <div class="login-form">
                @include('parts.messages')
                <form action="{{ url('/painel') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="cpf">CPF</label>
                        <input type="text" name="cpf" id="cpf" class="form-control" placeholder="CPF">
                    </div>
                    <div class="form-group">
                        <label>Senha</label>
                        <input type="password" name="senha" class="form-control" placeholder="Senha">
                    </div>
                    <button type="submit" class="btn btn-black">Entrar</button>
                    {{-- <button type="submit" class="btn btn-secondary">Register</button> --}}
                </form>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"></script>
    <script>
        $("#cpf").keydown(function(){
            try {
                $("#cpf").unmask();
            } catch (e) {}

            var tamanho = $("#cpf").val().length;

            $("#cpf").mask("999.999.999-99");

            // ajustando foco
            var elem = this;
            setTimeout(function(){
                // mudo a posição do seletor
                elem.selectionStart = elem.selectionEnd = 10000;
            }, 0);
            // reaplico o valor para mudar o foco
            var currentValue = $(this).val();
            $(this).val('');
            $(this).val(currentValue);
        });
    </script>
</body>
</html>