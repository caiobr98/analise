@extends('layouts.appbeneficiario')
@section('title') Dependentes @endsection
@section('content')
    <div class="">
        <ol class="breadcrumb">
            {{-- <li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home </a></li> --}}
            <li class="active">
                Dependentes
            </li>
        </ol>

        <div class="row">
            <div class="col-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Dependentes</h2>
                        {{-- <a class="pull-right btn btn-success" href="{{url('moleculas/create')}}">
                            Nova Molécula
                        </a>
                        <div class="clearfix"></div> --}}
                    </div>
                    <div class="x_content">
                        @include('parts.messages')

                        <table class="table table-striped data_table">
                            <thead>
                            <tr>
                                <th>Código</th>
                                <th>Nome</th>
                                <th>CPF</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($beneficiario as $b)
                                <tr>
                                    <td>{{$b->id}}</td>
                                    <td>{{$b->Nome}}</td>
                                    <td>{{$b->CPF}}</td>
                                    <td>
                                        <a class="btn btn-primary" href="{{ route('questao.index') }}">
                                            <i class="fa fa-edit"></i> Formulário
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
