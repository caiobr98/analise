<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes([
    //'register' => false,
    'verify' => false
]);

/*
|------------------------------------------------------------------------------------
| Beneficiário
|------------------------------------------------------------------------------------
*/
Route::namespace('Beneficiario')->group( function () {
    Route::resource('beneficiario', 'BeneficiarioController')->except(['show']);
    Route::post('/painel', 'BeneficiarioController@logar');
    Route::get('/beneficiario/dependentes/{id}', 'BeneficiarioController@dependentes')->name('painel');
});

/*
|------------------------------------------------------------------------------------
| Questão
|------------------------------------------------------------------------------------
*/
Route::namespace('Questao')->group( function () {
    Route::resource('questao', 'QuestaoController')->except(['show']);
});


Route::get('/home', 'HomeController@index');

